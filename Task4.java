public class Task4 {
    public static void main(String[] args) {
        int[] myArray = new int[8];
        boolean order = true;
        for (int i = 0; i  < myArray.length; i++) {
            myArray[i] = (int)(Math.random() * 10 + 1);
            if (i != 0 && myArray[i] <= myArray[i-1]) {
                order = false;
            }
        }
        for (int elem : myArray) {
            System.out.print(elem + " ");
        }
        System.out.println();
        if (order) {
            System.out.println("Массив строго возрастающий");
        } else {
            System.out.println("Массив не строго возрастающий");
        }
        for (int i = 1; i  < myArray.length; i+=2) {
            myArray[i] = 0;
        }
        for (int elem : myArray) {
            System.out.print(elem + " ");
        }
    }
}
